# -*- coding: utf-8 -*-

import logging

from flask import Blueprint, jsonify, request
from flasgger.utils import swag_from

from ai_new_api_server import app
from ai_new_api_server.builder import refund_query_builder


logging.basicConfig(level=logging.INFO)

_blueprint_name = 'refund'
_api_doc_dir = 'api_docs/' + _blueprint_name
refund = Blueprint(_blueprint_name, __name__, url_prefix='/refund')


@refund.route('/get_refund_candidates', methods=['POST'])
@swag_from(_api_doc_dir + '/get_refund_candidates.yml')
def get_refund_candidates_endpoint():
    params = request.get_json()
    result = {}
    try:
        logging.info(params)

        # required
        is_publisher = params['is_publisher']
        start_date_str = params['start_date']  # UTC
        end_date_str = params['end_date']  # UTC

        # optional
        cid = params.get('cid', '')
        oid = params.get('oid', '')
        inv = params.get('inv', '')
        tz = int(params.get('tz', '8'))
        result['data'] = refund_query_builder.get_refund_candidates(is_publisher, start_date_str, end_date_str, cid,
                                                                    oid, inv, tz)
    except Exception:
        app.logger.exception("Exception /get_refund_candidates")
        result = {}

    return jsonify(result)
