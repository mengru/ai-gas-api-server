# -*- coding: utf-8 -*-

from gevent import monkey
from gevent.pywsgi import WSGIServer
from ai_new_api_server import app
import argparse

monkey.patch_all()


def get_config():
    parser = argparse.ArgumentParser(description='rr-api-server')
    parser.add_argument('-p', '--port', type=int, default=5000, help="port")

    args = parser.parse_args()

    return args.port


def main():
    port = get_config()
    http = WSGIServer(('', port), app.wsgi_app)
    http.serve_forever()


if __name__ == '__main__':
    main()
