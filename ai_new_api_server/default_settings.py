DEBUG = False  # make sure DEBUG is off unless enabled explicitly otherwise
PRODUCTION = False  # turn off api document if set true

RR_PERF_HOST = 'localhost'
RR_PERF_PORT = 3306
RR_PERF_USER = 'root'
RR_PERF_PASSWORD = None
AI_RR_PERF_DATABASE = 'ai_rr_performance'

PERF_DATA_HOST = 'localhost'
PERF_DATA_PORT = 3306
PERF_DATA_USER = 'root'
PERF_DATA_PASSWORD = None
CAMP_CACHE_DATABASE = 'campaign_cache'
AI_ALL_ACTION_DATABASE = 'ai_all_action'
PERF_DATA_DATABASE = 'performance_data'
EXT_BUYING_DATABASE = 'external_buying'

DEEP_FUNNEL_HOST = 'localhost'
DEEP_FUNNEL_PORT = 3306
DEEP_FUNNEL_USER = 'root'
DEEP_FUNNEL_PASSWORD = None
AI_DEEP_FUNNEL_DATABASE = 'ai_deep_funnel'

AI_FRAUD_HOST = 'localhost'
AI_FRAUD_PORT = 3306
AI_FRAUD_USER = 'root'
AI_FRAUD_PASSWORD = None
AI_FRAUD_DATABASE = 'ai_fraud_detection'

S3_AI_CHARLIE_ACCESS_KEY = None
S3_AI_CHARLIE_SECRET_KEY = None
