# -*- coding: utf-8 -*-

from flask import Flask
from flask_compress import Compress
from flask_cors import CORS
from flasgger import Swagger
from ai_new_api_server import mysql_util

app = Flask(__name__)
Compress(app)
CORS(app)

app.config.from_object('ai_new_api_server.default_settings')
app.config.from_envvar('API_SETTINGS')

mysql_util.init_ai_rr_performance_db(
    host=app.config.get('RR_PERF_HOST'),
    port=app.config.get('RR_PERF_PORT'),
    user=app.config.get('RR_PERF_USER'),
    password=app.config.get('RR_PERF_PASSWORD'),
    database=app.config.get('AI_RR_PERF_DATABASE')
)

mysql_util.init_ai_all_action_db(
    host=app.config.get('PERF_DATA_HOST'),
    port=app.config.get('PERF_DATA_PORT'),
    user=app.config.get('PERF_DATA_USER'),
    password=app.config.get('PERF_DATA_PASSWORD'),
    database=app.config.get('AI_ALL_ACTION_DATABASE')
)

mysql_util.init_campaign_cache_db(
    host=app.config.get('PERF_DATA_HOST'),
    port=app.config.get('PERF_DATA_PORT'),
    user=app.config.get('PERF_DATA_USER'),
    password=app.config.get('PERF_DATA_PASSWORD'),
    database=app.config.get('CAMP_CACHE_DATABASE')
)

mysql_util.init_performance_data_db(
    host=app.config.get('PERF_DATA_HOST'),
    port=app.config.get('PERF_DATA_PORT'),
    user=app.config.get('PERF_DATA_USER'),
    password=app.config.get('PERF_DATA_PASSWORD'),
    database=app.config.get('PERF_DATA_DATABASE')
)

mysql_util.init_external_buying_db(
    host=app.config.get('PERF_DATA_HOST'),
    port=app.config.get('PERF_DATA_PORT'),
    user=app.config.get('PERF_DATA_USER'),
    password=app.config.get('PERF_DATA_PASSWORD'),
    database=app.config.get('EXT_BUYING_DATABASE')
)

mysql_util.init_ai_deep_funnel_db(
    host=app.config.get('DEEP_FUNNEL_HOST'),
    port=app.config.get('DEEP_FUNNEL_PORT'),
    user=app.config.get('DEEP_FUNNEL_USER'),
    password=app.config.get('DEEP_FUNNEL_PASSWORD'),
    database=app.config.get('AI_DEEP_FUNNEL_DATABASE')
)

mysql_util.init_ai_fraud_db(
    host=app.config.get('AI_FRAUD_HOST'),
    port=app.config.get('AI_FRAUD_PORT'),
    user=app.config.get('AI_FRAUD_USER'),
    password=app.config.get('AI_FRAUD_PASSWORD'),
    database=app.config.get('AI_FRAUD_DATABASE')
)

if not app.debug:
    import logging
    from logging.handlers import TimedRotatingFileHandler

    # https://docs.python.org/3.6/library/logging.handlers.html#timedrotatingfilehandler
    file_handler = TimedRotatingFileHandler('ai_new_api_server.log', 'midnight')
    file_handler.setLevel(logging.WARNING)
    file_handler.setFormatter(logging.Formatter('<%(asctime)s> <%(levelname)s> %(message)s'))
    app.logger.addHandler(file_handler)

from ai_new_api_server.refund import refund
app.register_blueprint(refund)

from ai_new_api_server.server_status import server_status
app.register_blueprint(server_status)

if app.config.get('PRODUCTION') is not True:
    description = '\n\n'.join([
        '* item1',
        '  * item1-1',
        '- item2',
    ])
    template = {
        "swagger": "2.0",
        "info": {
            "title": "Title of API documents",
            "description": description,
            "version": "0.0.1"
        },
    }

    swagger = Swagger(app, template=template)
