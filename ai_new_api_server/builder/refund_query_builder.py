"""
builders will get output from query agent, process it to certain format, and return to endpoint
"""
from ai_new_api_server.query_agent.refund_query_agent import RefundQueryAgent
from ai_new_api_server.util.date_util import date_str_interval_to_ts

refund_query_agent = RefundQueryAgent()


def get_refund_candidates(is_publisher, start_date_str, end_date_str, cid, oid, inv, tz):
    (start_ts, end_ts) = date_str_interval_to_ts(start_date_str, end_date_str, tz)
    data = refund_query_agent.query_refund_candidates(is_publisher, start_ts, end_ts, cid, oid, inv)
    return data
