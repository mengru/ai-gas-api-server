"""
1. dynamically bind parameters when needed
2. log complete query string via mysql dialect
"""
import logging
from ai_new_api_server import mysql_util
from sqlalchemy.sql import text
from sqlalchemy.dialects import mysql

logger = logging.getLogger('query_agent')


class RefundQueryAgent(object):
    def __init__(self):
        self.mysql_agent = mysql_util.get_ai_fraud_db_agent()

    def query_refund_candidates(self, is_publisher, start_ts, end_ts, cid, oid, inv):
        # determine table by inv_level
        inv_level = 'publisher' if is_publisher else 'app_id'
        table = 'refund_{}_performance'.format(inv_level)

        stmt = text("""
        SELECT
          *
        FROM
          {table}
        WHERE
          (time >= :start_ts AND time < :end_ts)
          {cid_condition}
          {oid_condition}
          {inv_condition}
        """.format(
            table=table,
            cid_condition='AND cid = :cid' if cid else '',
            oid_condition='AND oid = :oid' if oid else '',
            inv_condition='AND inv = :inv' if inv else ''
        ))

        # dynamical
        params = {
            'start_ts': start_ts,
            'end_ts': end_ts,
        }
        if cid:
            params['cid'] = cid
        if oid:
            params['oid'] = oid
        if inv:
            params['inv'] = inv

        stmt = stmt.bindparams(**params)

        logger.info(stmt.compile(dialect=mysql.dialect(), compile_kwargs={"literal_binds": True}))
        rows = self.mysql_agent.query_all(stmt)
        return [dict(row.items()) for row in rows]
