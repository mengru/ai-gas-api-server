from sqlalchemy import create_engine


class MySQLAgent(object):
    def __init__(self, host=None, port=None, user=None, password=None, database=None, cursor_type='dict'):
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.database = database
        self.cursor_type = cursor_type
        if host and port and user:
            is_server_side_cursor = cursor_type.startswith('ss_')
            self.engine = create_engine('mysql+pymysql://{user}:{password}@{host}:{port}/{db}'.format(
                host=self.host,
                port=self.port,
                user=self.user,
                password=self.password,
                db=self.database
            ), server_side_cursors=is_server_side_cursor)

    def query_one(self, query):
        conn = self.engine.connect()
        return conn.execute(query).fetchone()

    def query_all(self, query):
        conn = self.engine.connect()
        return conn.execute(query)
