from flask import Blueprint, jsonify


from ai_new_api_server import mysql_util
from sqlalchemy import exc

_blueprint_name = 'server_status'
_api_doc_dir = 'api_docs/' + _blueprint_name
server_status = Blueprint(_blueprint_name, __name__)


def _get_mysql_agents_status():
    sql_test_command = 'SELECT VERSION()'
    status_is_good = True
    for agent in mysql_util.get_all_db_agents():
        try:
            agent.query_one(sql_test_command)
        except exc.SQLAlchemyError:
            status_is_good = False
            break
    return status_is_good


@server_status.route('/status', methods=['GET'])
def get_server_status():
    status_is_good = _get_mysql_agents_status()
    response = jsonify(result=status_is_good)
    if not status_is_good:
        return response, 500
    return response


if __name__ == '__main__':
    print(_get_mysql_agents_status())
