
from ai_new_api_server.mysql_util.mysql_agent import MySQLAgent

campaign_cache_db_agent = MySQLAgent()
ai_rr_performance_db_agent = MySQLAgent()
all_action_db_agent = MySQLAgent()
performance_data_db_agent = MySQLAgent()
external_buying_db_agent = MySQLAgent()
ai_deep_funnel_db_agent = MySQLAgent()
ai_fraud_db_agent = MySQLAgent()


def init_campaign_cache_db(host, port, user, password, database):
    global campaign_cache_db_agent
    campaign_cache_db_agent = MySQLAgent(host, port, user, password, database)


def init_ai_all_action_db(host, port, user, password, database):
    global all_action_db_agent
    all_action_db_agent = MySQLAgent(host, port, user, password, database, cursor_type='ss_dict')


def init_ai_rr_performance_db(host, port, user, password, database):
    global ai_rr_performance_db_agent
    ai_rr_performance_db_agent = MySQLAgent(host, port, user, password, database)


def init_performance_data_db(host, port, user, password, database):
    global performance_data_db_agent
    performance_data_db_agent = MySQLAgent(host, port, user, password, database)


def init_external_buying_db(host, port, user, password, database):
    global external_buying_db_agent
    external_buying_db_agent = MySQLAgent(host, port, user, password, database)


def init_ai_deep_funnel_db(host, port, user, password, database):
    global ai_deep_funnel_db_agent
    ai_deep_funnel_db_agent = MySQLAgent(host, port, user, password, database)


def init_ai_fraud_db(host, port, user, password, database):
    global ai_fraud_db_agent
    ai_fraud_db_agent = MySQLAgent(host, port, user, password, database)


def get_campaign_cache_db_agent():
    return campaign_cache_db_agent


def get_ai_rr_performance_db_agent():
    return ai_rr_performance_db_agent


def get_all_action_db_agent():
    return all_action_db_agent


def get_performance_data_db_agent():
    return performance_data_db_agent


def get_external_buying_db_agent():
    return external_buying_db_agent


def get_ai_deep_funnel_db_agent():
    return ai_deep_funnel_db_agent


def get_ai_fraud_db_agent():
    return ai_fraud_db_agent


def get_all_db_agents():
    return [
        get_campaign_cache_db_agent(), get_ai_rr_performance_db_agent(), get_all_action_db_agent(),
        get_performance_data_db_agent(), get_external_buying_db_agent(), get_ai_deep_funnel_db_agent(),
        get_ai_fraud_db_agent(),
    ]
