VENV = venv

all: run

clean:
	rm -rf $(VENV) && rm -rf *.egg-info && rm -rf dist && rm -rf *.log* && rm -rf build

venv: requirements.txt setup.py
	virtualenv --python=python3 $(VENV) && $(VENV)/bin/pip install -r requirements.txt && $(VENV)/bin/python setup.py develop

run: venv
	API_SETTINGS=../settings.cfg $(VENV)/bin/ai-new-api-server -p 5566

build:
	python setup.py sdist
